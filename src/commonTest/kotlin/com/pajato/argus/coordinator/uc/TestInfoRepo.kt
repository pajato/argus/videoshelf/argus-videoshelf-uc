package com.pajato.argus.coordinator.uc

import com.pajato.argus.info.core.InfoId
import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoRepo
import com.pajato.argus.info.core.InfoType
import com.pajato.argus.info.core.InfoWrapper
import kotlinx.serialization.json.Json
import java.net.URI
import kotlin.test.fail

object TestInfoRepo : InfoRepo {
    override val cache: MutableMap<InfoKey, InfoWrapper> = mutableMapOf(getBonesInfo(), getHeartlandInfo())

    override suspend fun injectDependency(uri: URI) { TODO("Not yet implemented") }

    override suspend fun register(item: InfoWrapper) { TODO("Not yet implemented") }

    override suspend fun register(json: String) { TODO("Not yet implemented") }

    private fun getBonesInfo(): Pair<InfoKey, InfoWrapper> = getInfo("bones.json", 1911)

    private fun getHeartlandInfo(): Pair<InfoKey, InfoWrapper> = getInfo("heartland.json", 14929)

    private fun getInfo(name: String, id: InfoId): Pair<InfoKey, InfoWrapper> {
        val loader = this::class.java.classLoader
        val json = loader.getResource(name)?.readText() ?: fail("Resource $name not found")
        val item = Json.decodeFromString(InfoWrapper.serializer(), json)
        val key = InfoKey(InfoType.Tv.name, id)
        return Pair(key, item)
    }
}
