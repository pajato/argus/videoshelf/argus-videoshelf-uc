package com.pajato.argus.coordinator.uc

import com.pajato.argus.coordinator.core.Shelves
import com.pajato.argus.coordinator.core.Shelves.Finished
import com.pajato.argus.coordinator.core.Shelves.Next
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.coordinator.core.video.VideoRepo
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue
import kotlin.test.fail

internal const val EXPECTED_HISTORY_SIZE = 358

internal fun asserts(newVideo: Video, continueSize: Int = 0, pausedSize: Int = 0, finishedSize: Int = 0) {
    val historyRepo = getRepo(Shelves.History.name)
    val pausedRepo = getRepo(Shelves.Paused.name)
    val continueRepo = getRepo(Next.name)
    val finishedRepo = getRepo(Finished.name)
    val coordinator = TestCoordinatorRepo.cache[Next.name] ?: fail("Cached coordinator not found!")
    CoordinatorUseCases.markWatched(TestInfoRepo, TestCoordinatorRepo, coordinator, newVideo)
    assertEquals(EXPECTED_HISTORY_SIZE, historyRepo.cache.size)
    assertEquals(pausedSize, pausedRepo.cache.size)
    assertEquals(finishedSize, finishedRepo.cache.size)
    assertEquals(continueSize, continueRepo.cache.size)
    assertFalse(historyRepo.cache.values.contains(newVideo))
    assertTrue(historyRepo.cache.values.last().timestamp != 0L)
}

internal fun getRepo(id: String): VideoRepo {
    val coordinator = TestCoordinatorRepo.cache[id] ?: fail("Cached coordinator with id: '$id' not found!")
    return coordinator.repo
}
