package com.pajato.argus.coordinator.uc

import com.pajato.argus.coordinator.core.Timestamp
import com.pajato.argus.coordinator.core.shelf.Shelf
import com.pajato.argus.coordinator.core.video.Filter
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.coordinator.core.video.VideoRepo
import com.pajato.argus.info.core.InfoRepo
import com.pajato.persister.jsonFormat
import com.pajato.persister.readAndPruneData
import kotlinx.serialization.serializer
import java.net.URI

internal suspend fun createRepo(dir: URI, shelf: Shelf): VideoRepo {
    val repo = getRepo(dir, shelf)
    val uri = URI("$dir/${shelf.path}")
    repo.injectDependency(uri)
    return repo
}

private fun getRepo(dir: URI, shelf: Shelf): VideoRepo {
    return object : VideoRepo {
        private var repoUri: URI? = null
        override val cache: MutableMap<Timestamp, Video> = mutableMapOf()
        override val id: String = shelf.id
        override var selectedItem: Video? = null
        override suspend fun injectDependency(uri: URI) { loadCache(dir, shelf, cache).also { repoUri = uri } }
        override fun register(item: Video) { cache[item.timestamp] = item }
        override fun register(json: String) { register(jsonFormat.decodeFromString(Video.serializer(), json)) }
        override fun remove(item: Video) { cache.remove(item.timestamp) }
        override fun select(item: Video) { selectedItem = item }
        override fun filter(filter: Filter, infoRepo: InfoRepo): List<Video> = listOf()
        override fun filter(filter: Filter, infoRepo: InfoRepo, videos: List<Video>): List<Video> = listOf()
    }
}

private fun loadCache(dir: URI, shelf: Shelf, cache: MutableMap<Timestamp, Video>) {
    if (shelf.path.isEmpty()) return
    loadCache(URI("$dir/${shelf.path}"), cache)
}

private fun loadCache(uri: URI, cache: MutableMap<Timestamp, Video>) {
    fun getKeyFromItem(item: Video) = item.timestamp
    validateRepo(uri)
    readAndPruneData(uri, cache, serializer(), ::getKeyFromItem)
}
