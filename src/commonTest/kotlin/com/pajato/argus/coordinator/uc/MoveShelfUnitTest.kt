package com.pajato.argus.coordinator.uc

import com.pajato.dependency.uri.validator.INVALID_INJECT_ERROR
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import kotlin.test.fail

class MoveShelfUnitTest : ReportingTestProfiler() {
    private val loader: ClassLoader = this::class.java.classLoader
    private val repo = TestCoordinatorRepo
    private val list = repo.orderList

    @BeforeTest fun setup() {
        val url = loader.getResource("files") ?: fail(INVALID_INJECT_ERROR)
        copyResourceDirs(loader, "read-only-files", "files")
        runBlocking { TestCoordinatorRepo.injectDependencies(url.toURI(), TestInfoRepo) }
        assertEquals(TestCoordinatorRepo.cache.size, list.size, getOrderStateErrorMessage())
    }

    @Test fun `When moving the first shelf up, verify no changes`() {
        val originalState = list
        val id = list[0]
        CoordinatorUseCases.moveShelfUp(repo, id)
        assertEquals(originalState, list, "")
    }

    @Test fun `When moving the last shelf down, verify no changes`() {
        val original = list.toList()
        val lastIndex = list.size - 1
        val id = list[lastIndex]
        CoordinatorUseCases.moveShelfDown(repo, id)
        assertEquals(original, list, "")
    }

    @Test fun `When moving a shelf up (3 to 2), verify changes`() {
        val original = list.toList()
        val id = list[3] // history by default, 0 based
        CoordinatorUseCases.moveShelfUp(repo, id)
        assertNotEquals(original, list, "")
    }

    @Test fun `When maving a shelf down (2 to 3), verify changes`() {
        val originalState = list.toList()
        val id = list[2]
        CoordinatorUseCases.moveShelfDown(repo, id)
        assertNotEquals(originalState, list, "")
    }

    private fun getOrderStateErrorMessage() = "Order repo has wrong size!"
}
