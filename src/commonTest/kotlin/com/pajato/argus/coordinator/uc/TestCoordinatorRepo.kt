package com.pajato.argus.coordinator.uc

import com.pajato.argus.coordinator.core.Coordinator
import com.pajato.argus.coordinator.core.CoordinatorError
import com.pajato.argus.coordinator.core.CoordinatorRepo
import com.pajato.argus.coordinator.core.ErrorStrings.COORDINATOR_INJECTION_ERROR
import com.pajato.argus.coordinator.core.ErrorStrings.COORDINATOR_NOT_FOUND
import com.pajato.argus.coordinator.core.shelf.Shelf
import com.pajato.argus.coordinator.core.video.VideoRepo
import com.pajato.argus.info.core.InfoRepo
import com.pajato.i18n.strings.Arg
import com.pajato.i18n.strings.StringsResource.get
import com.pajato.persister.jsonFormat
import com.pajato.persister.persist
import com.pajato.persister.readAndPruneData
import java.net.URI

object TestCoordinatorRepo : CoordinatorRepo {
    override val cache: MutableMap<String, Coordinator> = mutableMapOf()
    override val orderList: MutableList<String> = mutableListOf()

    override suspend fun injectDependencies(dir: URI, infoRepo: InfoRepo) {
        val shelfCache: MutableMap<String, Shelf> = mutableMapOf()
        setupCache(dir, shelfCache)
        setupOrderList()
    }

    private suspend fun setupCache(dir: URI, shelfCache: MutableMap<String, Shelf>) {
        validateDir(dir).also { shelfUri = URI("$dir/shelf.txt") }
        readAndPruneData(shelfUri!!, shelfCache, Shelf.serializer(), ::getIdFromShelf)
        shelfCache.values.forEach { cache[it.id] = Coordinator(it, createRepo(dir, it)) }
    }

    private fun setupOrderList() {
        orderList.clear()
        orderList.addAll(MutableList(cache.size) { "" })
        cache.values.forEach { validateOrder(it.shelf) }
    }

    private fun validateOrder(shelf: Shelf) {
        val index = shelf.order
        if (orderList.contains(shelf.id)) throw CoordinatorError("shelf does not have unique order!")
        orderList[index] = shelf.id
    }

    override fun swapWithShelfAbove(id: String) {
        val index = orderList.indexOf(id)
        if (index > 0) swapOrder(index, index - 1)
    }

    override fun swapWithShelfBelow(id: String) {
        val lastIndex = orderList.size - 1
        val index = orderList.indexOf(id)
        if (index < lastIndex) swapOrder(index, index + 1)
    }

    private fun swapOrder(index1: Int, index2: Int) {
        val (id1, id2) = Pair(orderList[index1], orderList[index2])
        orderList[index1] = id2
        orderList[index2] = id1
        update(id1, index2)
        update(id2, index1)
    }

    private fun update(id: String, index: Int) {
        val entry = cache[id] ?: throw CoordinatorError(get(COORDINATOR_INJECTION_ERROR))
        val shelf = entry.shelf.copy(order = index)
        cache[id] = entry.copy(shelf = shelf)
        persistShelf(shelf)
    }

    private fun persistShelf(shelf: Shelf) {
        val json = jsonFormat.encodeToString(Shelf.serializer(), shelf)
        val uri: URI = shelfUri ?: throw CoordinatorError(get(COORDINATOR_INJECTION_ERROR))
        persist(uri, json)
    }

    override fun getShelf(id: String): Shelf =
        (cache[id] ?: throw CoordinatorError(get(COORDINATOR_NOT_FOUND, Arg("id", id)))).shelf

    override fun getVideoRepo(id: String): VideoRepo =
        (cache[id] ?: throw CoordinatorError(get(COORDINATOR_NOT_FOUND, Arg("id", id)))).repo

    private var shelfUri: URI? = null

    private fun getIdFromShelf(shelf: Shelf): String = shelf.id

    internal fun reset() {
        cache.clear()
        shelfUri = null
    }
}
