package com.pajato.argus.coordinator.uc

import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.coordinator.uc.CoordinatorUseCases.formatTimestamp
import com.pajato.argus.coordinator.uc.CoordinatorUseCases.toTimestamp
import java.text.ParsePosition
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class TimestampUnitTest {

    @Test fun `When formatting a timestamp, verify behavior`() {
        val video = Video(0L)
        val expected = "31-Dec-1969 07:00 PM"
        assertEquals(expected, formatTimestamp(video))
    }

    @Test fun `When converting an empty string, verify behavior`() {
        val errors = ParsePosition(0)
        assertTrue("".toTimestamp(errors) == null)
        assertEquals(0, errors.errorIndex)
    }

    @Test fun `When converting an invalid date-time string, verify behavior`() {
        val errors = ParsePosition(0)
        assertTrue("1x".toTimestamp(errors) == null)
        assertEquals(1, errors.errorIndex)
    }

    @Test fun `When converting a valid date-time string, verify behavior`() {
        val errors = ParsePosition(0)
        assertEquals(0L, "31-Dec-1969 07:00 PM".toTimestamp(errors))
        assertEquals(-1, errors.errorIndex)
    }
}
