plugins {
    alias(libs.plugins.kmp.lib)
}

group = "com.pajato.argus"
version = "0.10.5"
description = "The Argus video-shelf feature, use-case (uc) layer, KMP common target project"

kotlin.sourceSets {
    val commonMain by getting {
        dependencies {
            implementation(libs.pajato.i18n.strings)
            implementation(libs.pajato.persister)
            implementation(libs.pajato.uri.validator)

            implementation(libs.argus.info.core)
            implementation(libs.argus.info.uc)
            implementation(libs.argus.videoShelf.core)
        }
    }

    val commonTest by getting {
        dependencies {
            implementation(libs.kotlinx.coroutines.test)
            implementation(libs.kotlinx.serialization.json)
            implementation(libs.kotlin.test)
            implementation(libs.pajato.test)
        }
    }
}
