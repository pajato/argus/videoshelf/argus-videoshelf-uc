package com.pajato.argus.coordinator.uc

import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.coordinator.core.video.VideoRepo
import com.pajato.argus.coordinator.uc.CoordinatorUseCases.selectVideo
import com.pajato.argus.coordinator.uc.TestVideoRepo.selectedItem
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import java.io.File
import java.net.URL
import kotlin.io.path.toPath
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class SelectUnitTest : ReportingTestProfiler() {
    private val testVideoRepo: VideoRepo = TestVideoRepo
    private val failMessage = "Expected video resource not found!"
    private lateinit var file: File

    @BeforeTest fun setUp() {
        val name = "read-only-files/history.txt"
        val url: URL = this::class.java.classLoader.getResource(name) ?: fail(failMessage)
        file = url.toURI().toPath().toFile()
        runBlocking { testVideoRepo.injectDependency(url.toURI()) }
    }

    @Test fun `When a video is selected, verify the selected video id`() = runBlocking {
        selectVideo(testVideoRepo, Video(-1L, 0))
        assertVideo()
    }

    private fun assertVideo() {
        val video = selectedItem ?: fail("The selected video should not be null!")
        assertEquals(0, video.infoId)
    }
}
