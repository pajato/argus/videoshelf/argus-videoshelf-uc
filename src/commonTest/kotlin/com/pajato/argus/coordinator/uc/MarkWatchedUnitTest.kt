package com.pajato.argus.coordinator.uc

import com.pajato.argus.coordinator.core.Shelves
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import java.net.URI
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue
import kotlin.test.fail

class MarkWatchedUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader

    @BeforeTest fun setUp() {
        val name = "files"
        val dir: URI = loader.getResource(name)?.toURI() ?: fail("Resource dir 'files' could not be accessed!")
        copyResourceDirs(loader, "read-only-files", "files")
        runBlocking { TestCoordinatorRepo.injectDependencies(dir, TestInfoRepo) }
    }

    @Test fun `When a non-continue shelf video is mark-watched, verify only history register action`() {
        val newVideo = Video(0L)
        val coordinator = TestCoordinatorRepo.cache[Shelves.History.name] ?: fail("Cached coordinator not found!")
        val repo = coordinator.repo
        CoordinatorUseCases.markWatched(TestInfoRepo, TestCoordinatorRepo, coordinator, newVideo)
        assertEquals(EXPECTED_HISTORY_SIZE, repo.cache.size)
        assertFalse(repo.cache.values.contains(newVideo))
        assertTrue(repo.cache.values.last().timestamp != 0L)
    }

    @Test fun `When continuing a last episode, verify finished, continue and history actions`() {
        val newVideo = Video(0L, 1911, 12, 12, 0, 0)
        asserts(newVideo, continueSize = 0, pausedSize = 0, finishedSize = 1)
    }

    @Test fun `When continuing a last returning episode, verify paused, continue and history actions`() {
        val newVideo = Video(0L, 14929, 10, 17, 0, 0)
        asserts(newVideo, continueSize = 0, pausedSize = 1)
    }

    @Test fun `When continuing a last episode in series, verify continue and history actions`() {
        val newVideo = Video(0L, 1911, 22, 1, 0, 0)
        asserts(newVideo, continueSize = 1)
    }

    @Test fun `When continuing the next episode in series, verify continue and history actions`() {
        val newVideo = Video(0L, 1911, 14, 1, 0, 0)
        asserts(newVideo, continueSize = 1)
    }

    @Test fun `When marking an episode as watched, verify no other cache entries are affected`() {
        val id = "Next"
        val v1 = Video(1L, 1911, 3, 2, 0, 0)
        val v2 = Video(2L, 14929, 7, 5, 0, 0)
        getRepo(id).register(v1)
        getRepo(id).register(v2)
        asserts(v1, continueSize = 2)
    }
}
