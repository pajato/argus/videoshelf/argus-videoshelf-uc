package com.pajato.argus.coordinator.uc

object TestAgent {
    const val REPO_REGISTER_FAILURE = "The repo register code did not complete successfully!"
    const val LOAD_VIDEOS_FAILURE = "Could not load networks resource file!"
    const val ADD_CACHE_FAILURE = "The repo cache does not contain the added item!"
    const val ADD_PERSIST_FAILURE = "The JSON text was not appended to the persistence file!"
}
