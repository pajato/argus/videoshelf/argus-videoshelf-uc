package com.pajato.argus.coordinator.uc

import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.coordinator.core.video.VideoRepo
import com.pajato.argus.coordinator.uc.CoordinatorUseCases.removeAllVideos
import com.pajato.argus.coordinator.uc.CoordinatorUseCases.removeVideo
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.test.fail

class RemoveVideoUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader
    private val url = loader.getResource("files/history.txt") ?: fail(TestAgent.LOAD_VIDEOS_FAILURE)
    private val testVideoRepo: VideoRepo = TestVideoRepo
    private val testVideoTimestamp = 23L

    @BeforeTest fun setUp() {
        copyResourceDirs(loader, "read-only-files", "files")
        runBlocking { TestVideoRepo.injectDependency(url.toURI()) }
        runBlocking { testVideoRepo.injectDependency(url.toURI()) }
    }

    @Test fun `When a new video is removed, verify the remover and cacheable behaviors`() = runBlocking {
        removeVideo(testVideoRepo, Video(testVideoTimestamp))
        assertTrue(TestVideoRepo.removeQueued)
        assertTrue(testVideoRepo.cache[testVideoTimestamp] == null)
    }

    @Test fun `When all videos are removed, verify the cache is empty`() = runBlocking {
        removeAllVideos(testVideoRepo, url.toURI())
        assertTrue(TestVideoRepo.removeQueued)
        assertEquals(0, testVideoRepo.cache.size)
    }
}
