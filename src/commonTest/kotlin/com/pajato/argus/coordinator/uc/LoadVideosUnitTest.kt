package com.pajato.argus.coordinator.uc

import com.pajato.argus.coordinator.uc.CoordinatorUseCases.loadVideos
import com.pajato.argus.coordinator.uc.TestAgent.LOAD_VIDEOS_FAILURE
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class LoadVideosUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader
    private val url = loader.getResource("files/history.txt") ?: fail(LOAD_VIDEOS_FAILURE)

    @BeforeTest fun setUp() {
        copyResourceDirs(loader, "read-only-files", "files")
        runBlocking { TestVideoRepo.injectDependency(url.toURI()) }
    }

    @Test fun `When initializing using test data, verify the repo size`() {
        runBlocking { loadVideos(TestVideoRepo, url.toURI()) }
        assertEquals(357, TestVideoRepo.cache.size)
    }
}
