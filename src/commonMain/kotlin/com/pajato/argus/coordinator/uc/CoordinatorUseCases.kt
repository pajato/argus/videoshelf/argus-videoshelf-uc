package com.pajato.argus.coordinator.uc

import com.pajato.argus.coordinator.core.Coordinator
import com.pajato.argus.coordinator.core.CoordinatorRepo
import com.pajato.argus.coordinator.core.Shelves
import com.pajato.argus.coordinator.core.Timestamp
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.coordinator.core.video.VideoRepo
import com.pajato.argus.info.core.InfoRepo
import java.io.File
import java.net.URI
import java.text.ParsePosition
import java.text.SimpleDateFormat
import java.util.Date
import java.util.TimeZone

public object CoordinatorUseCases {
    public suspend fun loadVideos(repo: VideoRepo, uri: URI) { repo.injectDependency(uri) }

    public fun addVideo(repo: VideoRepo, video: Video) { repo.register(video) }

    public fun addAllVideos(repo: VideoRepo, videos: List<Video>) { videos.forEach { repo.register(it) } }

    public fun removeVideo(repo: VideoRepo, video: Video) { repo.remove(video) }

    public fun removeAllVideos(repo: VideoRepo, uri: URI) {
        repo.cache.clear()
        File(uri).writeText("")
    }

    public fun modifyVideo(repo: VideoRepo, video: Video, newVideo: Video) { modifyHelper(repo, video, newVideo) }

    public fun getVideo(repo: VideoRepo, timestamp: Timestamp): Video? = repo.cache[timestamp]

    public fun getAllVideos(repo: VideoRepo): List<Video> = repo.cache.values.toList()

    public fun selectVideo(repo: VideoRepo, video: Video): Unit = repo.select(video)

    private const val DATE_PATTERN = "d-MMM-yyyy hh:mm a"

    public fun formatTimestamp(video: Video): String {
        val formatter = SimpleDateFormat(DATE_PATTERN)
        formatter.timeZone = TimeZone.getDefault()
        return formatter.format(Date(video.timestamp))
    }

    public fun String.toTimestamp(errors: ParsePosition): Timestamp? =
        SimpleDateFormat(DATE_PATTERN).parse(this, errors)?.time

    public fun getRepo(repo: CoordinatorRepo, id: String): VideoRepo = repo.getVideoRepo(id)

    public fun markWatched(infoRepo: InfoRepo, repo: CoordinatorRepo, coordinator: Coordinator, video: Video) {
        val newVideo = video.copy(System.currentTimeMillis())
        fun isContinue() = coordinator.shelf.id == Shelves.Next.name
        register(repo, newVideo)
        if (isContinue()) updateContinue(Pair(infoRepo, repo), Pair(video, newVideo))
    }

    public fun moveShelfUp(repo: CoordinatorRepo, id: String) { repo.swapWithShelfAbove(id) }

    public fun moveShelfDown(repo: CoordinatorRepo, id: String) { repo.swapWithShelfBelow(id) }
}
