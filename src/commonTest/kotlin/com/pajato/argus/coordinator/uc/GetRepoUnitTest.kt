package com.pajato.argus.coordinator.uc

import com.pajato.argus.coordinator.core.Shelves
import com.pajato.argus.coordinator.uc.CoordinatorUseCases.getRepo
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import java.net.URI
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class GetRepoUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader

    @BeforeTest fun setUp() {
        val name = "files"
        val dir: URI = loader.getResource(name)?.toURI() ?: fail("Resource dir 'files' could not be accessed!")
        copyResourceDirs(loader, "read-only-files", "files")
        runBlocking { TestCoordinatorRepo.injectDependencies(dir, TestInfoRepo) }
    }

    @Test fun `When accessing the history video repo, verify a few things`() {
        val expected = Shelves.History.name
        val repo = getRepo(TestCoordinatorRepo, Shelves.History.name)
        assertEquals(expected, repo.id)
    }
}
