package com.pajato.argus.coordinator.uc

import com.pajato.argus.coordinator.core.CoordinatorError
import com.pajato.argus.coordinator.core.ErrorStrings.COORDINATOR_INJECTION_ERROR
import com.pajato.argus.coordinator.core.Timestamp
import com.pajato.argus.coordinator.core.video.Filter
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.coordinator.core.video.VideoRepo
import com.pajato.argus.info.core.InfoRepo
import com.pajato.dependency.uri.validator.validateUri
import com.pajato.i18n.strings.StringsResource.get
import com.pajato.persister.jsonFormat
import com.pajato.persister.persist
import com.pajato.persister.readAndPruneData
import java.net.URI

object TestVideoRepo : VideoRepo {
    override val cache: MutableMap<Timestamp, Video> = mutableMapOf()
    override val id: String get() = TODO("Not yet implemented")
    override var selectedItem: Video? = null
    override fun filter(filter: Filter, infoRepo: InfoRepo): List<Video> { TODO("Not yet implemented") }

    override fun filter(filter: Filter, infoRepo: InfoRepo, videos: List<Video>): List<Video> {
        TODO("Not yet implemented")
    }

    private var videoUri: URI? = null

    internal var addQueued = false
    internal var removeQueued = true

    override suspend fun injectDependency(uri: URI) {
        fun handler(cause: Throwable) { throw cause }
        validateUri(uri, ::handler)
        readAndPruneData(uri, cache, Video.serializer(), TestVideoRepo::getKeyFromItem)
        videoUri = uri
    }

    override fun register(item: Video) {
        val uri = videoUri ?: throw CoordinatorError(get(COORDINATOR_INJECTION_ERROR))
        val json = jsonFormat.encodeToString(Video.serializer(), item)
        cache[item.timestamp] = item
        persist(uri, json)
        addQueued = true
    }

    override fun register(json: String) {
        val uri = videoUri ?: throw CoordinatorError(get(COORDINATOR_INJECTION_ERROR))
        val item = jsonFormat.decodeFromString(Video.serializer(), json)
        cache[item.timestamp] = item
        persist(uri, json)
        addQueued = true
    }

    override fun remove(item: Video) {
        val uri = videoUri ?: throw CoordinatorError(get(COORDINATOR_INJECTION_ERROR))
        val json = "-${jsonFormat.encodeToString(Video.serializer(), item)}"
        cache.remove(item.timestamp)
        persist(uri, json)
        removeQueued
    }

    override fun select(item: Video) { selectedItem = item }

    private fun getKeyFromItem(item: Video) = item.timestamp
}
