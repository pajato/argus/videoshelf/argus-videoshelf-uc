package com.pajato.argus.coordinator.uc

import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.coordinator.core.video.VideoRepo
import com.pajato.argus.coordinator.uc.CoordinatorUseCases.addAllVideos
import com.pajato.argus.coordinator.uc.CoordinatorUseCases.addVideo
import com.pajato.argus.coordinator.uc.TestAgent.ADD_CACHE_FAILURE
import com.pajato.argus.coordinator.uc.TestAgent.ADD_PERSIST_FAILURE
import com.pajato.argus.coordinator.uc.TestAgent.LOAD_VIDEOS_FAILURE
import com.pajato.argus.coordinator.uc.TestAgent.REPO_REGISTER_FAILURE
import com.pajato.persister.jsonFormat
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import java.io.File
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.test.fail

class AddVideoUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader
    private val testVideoRepo: VideoRepo = TestVideoRepo
    private val testVideoTimestamp = 23L
    private lateinit var repoFile: File

    @BeforeTest fun setUp() {
        val url = loader.getResource("files/history.txt") ?: fail(LOAD_VIDEOS_FAILURE)
        copyResourceDirs(loader, "read-only-files", "files")
        runBlocking { TestVideoRepo.injectDependency(url.toURI()) }
        repoFile = File(url.toURI())
    }

    @Test fun `When a new video is added, verify persistence and caching behaviors`() {
        val videoToAdd = Video(testVideoTimestamp)
        val json = jsonFormat.encodeToString(Video.serializer(), videoToAdd)
        runBlocking { addVideo(testVideoRepo, videoToAdd) }
        assertTrue(TestVideoRepo.addQueued, REPO_REGISTER_FAILURE)
        assertTrue(TestVideoRepo.cache.containsKey(videoToAdd.timestamp), ADD_CACHE_FAILURE)
        assertEquals(json, repoFile.readLines().last { it.isNotEmpty() }, ADD_PERSIST_FAILURE)
    }

    @Test fun `When new videos are added, verify persistence and caching behaviors`() {
        val videosToAdd: List<Video> = listOf(Video(testVideoTimestamp))
        val json = jsonFormat.encodeToString(Video.serializer(), videosToAdd[0])
        runBlocking { addAllVideos(testVideoRepo, videosToAdd) }
        assertTrue(TestVideoRepo.addQueued, REPO_REGISTER_FAILURE)
        assertTrue(TestVideoRepo.cache.containsKey(videosToAdd[0].timestamp), ADD_CACHE_FAILURE)
        assertEquals(json, repoFile.readLines().last { it.isNotEmpty() }, ADD_PERSIST_FAILURE)
    }
}
