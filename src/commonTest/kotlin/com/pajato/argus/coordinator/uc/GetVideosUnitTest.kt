package com.pajato.argus.coordinator.uc

import com.pajato.argus.coordinator.uc.CoordinatorUseCases.getAllVideos
import com.pajato.argus.coordinator.uc.CoordinatorUseCases.getVideo
import com.pajato.argus.coordinator.uc.TestAgent.LOAD_VIDEOS_FAILURE
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.test.fail

class GetVideosUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader

    @BeforeTest fun setUp() {
        val url = loader.getResource("files/history.txt") ?: fail(LOAD_VIDEOS_FAILURE)
        copyResourceDirs(loader, "read-only-files", "files")
        runBlocking { TestVideoRepo.injectDependency(url.toURI()) }
    }

    @Test fun `When accessing a video with an invalid key, verify null result`() {
        assertTrue { getVideo(TestVideoRepo, 0) == null }
    }

    @Test fun `When accessing a valid video, verify correct video timestamp`() {
        val failMessage = "Invalid (null) video!"
        val video = getVideo(TestVideoRepo, -1L) ?: fail(failMessage)
        assertEquals(-1L, video.timestamp)
    }

    @Test fun `When accessing all videos, verify correct size`() {
        assertTrue(getAllVideos(TestVideoRepo).size >= 9)
    }
}
