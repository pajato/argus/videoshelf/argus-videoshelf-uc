package com.pajato.argus.coordinator.uc

import com.pajato.argus.coordinator.core.CoordinatorError
import com.pajato.argus.coordinator.core.CoordinatorRepo
import com.pajato.argus.coordinator.core.ErrorStrings.COORDINATOR_TIMESTAMP_ERROR
import com.pajato.argus.coordinator.core.Shelves.History
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.coordinator.core.video.VideoRepo
import com.pajato.argus.coordinator.uc.CoordinatorUseCases.getRepo
import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoRepo
import com.pajato.argus.info.core.InfoType
import com.pajato.i18n.strings.StringsResource.get

public typealias Repos = Pair<InfoRepo, CoordinatorRepo>

internal fun register(repo: CoordinatorRepo, video: Video) = getRepo(repo, History.name).register(video)

internal fun updateContinue(repos: Pair<InfoRepo, CoordinatorRepo>, videos: Pair<Video, Video>) {
    val originalVideo = videos.first
    val infoKey = InfoKey(InfoType.Tv.name, originalVideo.infoId)
    handleWhen(repos, infoKey, originalVideo, videos)
}

private fun handleWhen(repos: Repos, infoKey: InfoKey, video: Video, videos: Pair<Video, Video>) = when {
    isLastEpisode(repos, infoKey, video) -> registerLastEpisode(repos.second, videos)
    isLastReturningEpisode(repos, infoKey, video) -> registerLastReturningEpisode(repos.second, videos)
    isLastEpisodeInSeries(repos, infoKey, video) -> registerLastEpisodeInSeries(repos.second, videos)
    else -> registerNextEpisode(repos.second, videos)
}

internal fun modifyHelper(repo: VideoRepo, video: Video, newVideo: Video) {
    fun isSameKey(): Boolean = video.timestamp == newVideo.timestamp
    if (isSameKey()) repo.register(newVideo) else repo.replace(video, newVideo)
}

private fun VideoRepo.replace(video: Video, newVideo: Video) {
    if (cache.containsKey(newVideo.timestamp)) throw CoordinatorError(get(COORDINATOR_TIMESTAMP_ERROR))
    remove(video)
    register(newVideo)
}
