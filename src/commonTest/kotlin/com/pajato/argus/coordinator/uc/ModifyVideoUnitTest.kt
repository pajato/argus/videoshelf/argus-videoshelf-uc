package com.pajato.argus.coordinator.uc

import com.pajato.argus.coordinator.core.CoordinatorError
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.coordinator.core.video.VideoRepo
import com.pajato.argus.coordinator.uc.CoordinatorUseCases.modifyVideo
import com.pajato.argus.coordinator.uc.TestAgent.LOAD_VIDEOS_FAILURE
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertFalse
import kotlin.test.assertTrue
import kotlin.test.fail

class ModifyVideoUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader
    private val testVideoRepo: VideoRepo = TestVideoRepo
    private val testVideoTimestamp = 23L

    @BeforeTest fun setUp() {
        val url = loader.getResource("files/history.txt") ?: fail(LOAD_VIDEOS_FAILURE)
        copyResourceDirs(loader, "read-only-files", "files")
        runBlocking { TestVideoRepo.injectDependency(url.toURI()) }
    }

    @Test fun `When no changes are made to (cancel operation), verify no changes`() {
        val failMessage = "Invalid (null) video!"
        val video = Video(testVideoTimestamp)
        modifyVideo(testVideoRepo, video, video)
        testVideoRepo.cache[testVideoTimestamp]?.let { assertVideoUnmodified(it) } ?: fail(failMessage)
    }

    @Test fun `When overwriting an existing timestamp via modify, verify a coordinator error is thrown`() {
        val video = Video(0L)
        val newVideo = Video(-1L)
        assertFailsWith<CoordinatorError> { modifyVideo(TestVideoRepo, video, newVideo) }
    }

    @Test fun `When replacing an existing timestamp via modify, verify behavior coordinator error is thrown`() {
        val video = Video(0L)
        val newVideo = Video(1L)
        modifyVideo(TestVideoRepo, video, newVideo)
        assertFalse(TestVideoRepo.cache.containsKey(video.timestamp))
        assertTrue(TestVideoRepo.cache.containsKey(newVideo.timestamp))
    }

    private fun assertVideoUnmodified(actual: Video) { assertEquals(testVideoTimestamp, actual.timestamp) }
}
