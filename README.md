# argus-coordinator-uc

## Description

This project implements the [Clean Code
Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html) "Application Business Rules"
(aka Use Cases) layer for the coordinator feature. The project is responsible for development of use cases for the
coordinator feature.

The sole responsibility for the coordinator feature is to support the Argus application's ability to present
collections of videos matching a theme, presented in a "shelf". These themes include such things as continue watching,
watchlist, history, paused, finished and hidden.

## License

GPL, V3, See the peer document LICENSE for details.

## Contributions

See the [contributing guide](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/CONTRIBUTING.md) in the
[Argus Documentation Project](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/README.md).

## Project status

Converted to Kotlin Multiplatform (KMP) with versions 0.10.*

## Documentation

For general documentation on Argus, see the [Argus Documentation Project](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/README.md).

As documentation entered into code files grows stale seconds after it is written, no such documentation is created.
Instead, documentation is created by you on demand using the Dokka Gradle task: 'dokkaGfm'. After successful task
completion, see the detailed documentation [here](build/dokka/gfm/index.md)

## Usage

To use the project, follow these steps:

1. Add the project as a dependency in your build file.
2. Import the necessary classes and interfaces from the project.
3. Use the provided APIs to interact with the shelf feature.

## Test Cases

### Overview

The table below identifies the adapter layer unit tests. A test file name is always of the form `NamePrefixUnitTest.kt`.
The test file content is one or more test cases (functions)

| Filename Prefix | Test Case Name                                                                          |
|-----------------|-----------------------------------------------------------------------------------------|
| AddUnit         | When a new video is added, verify persistence and caching behaviors                     |
|                 | When new videos are added, verify persistence and caching behaviors                     |
| GetVideo        | When accessing a video with an invalid key, verify null result                          |
|                 | When accessing a valid video, verify correct video timestamp                            |
|                 | When accessing all videos, verify correct size                                          |
| LoadVideos      | When initializing using test data, verify the repo size                                 |
| ModifyVideo     | When no changes are made to (cancel operation), verify no changes                       |
|                 | When overwriting an existing timestamp via modify, verify a coordinator error is thrown |
|                 | When replacing a timestamp via modify, verify behavior                                  |
| RemoveVideo     | When a new video is removed, verify the remover and cacheable behaviors                 |
|                 | When all videos are removed, verify the cache is empty                                  |
| Select          | When a video is selected, verify the selected video id                                  |
| Timestamp       | When converting an empty string, verify behavior                                        |
|                 | When converting an invalid date-time string, verify behavior                            |
|                 | When converting a valid date-time string, verify behavior                               |
| MarkWatched     | When a non-continue shelf video is mark-watched, verify only history register action    |
|                 | When continuing a last episode, verify finished, continue and history actions           |
|                 | When continuing a last returning episode, verify paused, continue and history actions   |
|                 | When continuing a last episode in series, verify continue and history actions           |
|                 | When continuing the next episode in series, verify continue and history actions         |
| MoveShelf       | When moving the first shelf up, verify no changes                                       |
|                 | When moving the last shelf down, verify no changes                                      |
|                 | When moving a shelf up (3 to 2), verify changes                                         |
|                 | When moving a shelf down (2 to 3), verify changes                                       |

### Notes

The Argus video shelf Use Cases at a high level are:

- Manage videos (load, add, hide, finish, pause, select-toggle, select-one, show list, show details, edit)
+ addVideo(VideoRepo, Video)
+ removeVideo(VideoRepo, Video)
+ selectVideo(Video)
+ getVideos(VideoRepo): List<Video>
+ getVideo(VideoId, VideoRepo): Video
+ markWatched(Coordinator, Video)

- Manage networks, aka watch providers (add, show details, edit)
- Manage genres (select-toggle, select-multiple, show list)
- Manage watchlist (add, remove, watch)
- Show Video History List (with filtering for videos and genres; find by text)
- Show Continue Watching List (with filtering for videos and genres; find by text)
- Show Video Details (movie, tv show, tv series, tv episode)
- Show Cast List (...)
- Show Crew List (...)
- Show Episode Guest Star List (...)
- Show Cast Details (including filmography)
- Show Crew Details (including filmography)
- Show Popular Videos (fitered by genre)
- Show Top Rated Videos (filtered by genre)
- Search (top level for videos)
