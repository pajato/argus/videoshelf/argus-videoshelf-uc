package com.pajato.argus.coordinator.uc

import com.pajato.argus.coordinator.core.CoordinatorError
import com.pajato.argus.coordinator.core.ErrorStrings.COORDINATOR_DIR_ERROR
import com.pajato.argus.coordinator.core.ErrorStrings.COORDINATOR_EMPTY_ERROR
import com.pajato.argus.coordinator.core.ErrorStrings.COORDINATOR_EXISTS_ERROR
import com.pajato.argus.coordinator.core.ErrorStrings.COORDINATOR_NOT_A_FILE
import com.pajato.dependency.uri.validator.validateUri
import com.pajato.i18n.strings.Arg
import com.pajato.i18n.strings.StringsResource.get
import java.io.File
import java.net.URI
import kotlin.io.path.toPath

internal fun validateDir(dir: URI) {
    val uri = "$dir/shelf.txt"
    if (dir.toPath().toFile().isDirectory) validateShelf(uri) else throwDirError(dir)
}

private fun validateShelf(uri: String) {
    val shelfUri = URI(uri)
    if (!File(shelfUri).exists()) throw CoordinatorError(get(COORDINATOR_EXISTS_ERROR, Arg("path", uri)))
    validateUri(shelfUri) { handleError(uri) }
    if (shelfUri.toURL().readText().isEmpty()) throw CoordinatorError(get(COORDINATOR_EMPTY_ERROR))
}

private fun throwDirError(dir: URI) { throw CoordinatorError(get(COORDINATOR_DIR_ERROR, Arg("dir", "$dir"))) }

private fun handleError(uri: String) { throw CoordinatorError(get(COORDINATOR_NOT_A_FILE, Arg("path", uri))) }

internal fun validateRepo(uri: URI) { validateUri(uri) { handleError(uri.path) } }
