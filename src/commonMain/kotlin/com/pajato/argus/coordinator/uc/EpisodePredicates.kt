package com.pajato.argus.coordinator.uc

import com.pajato.argus.coordinator.core.CoordinatorRepo
import com.pajato.argus.coordinator.core.Shelves.Finished
import com.pajato.argus.coordinator.core.Shelves.Next
import com.pajato.argus.coordinator.core.Shelves.Paused
import com.pajato.argus.coordinator.core.video.Video
import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoRepo
import com.pajato.argus.info.uc.TvUseCases
import com.pajato.argus.info.uc.TvUseCases.isLastEpisodeInSeries

internal fun isLastEpisode(repos: Pair<InfoRepo, CoordinatorRepo>, key: InfoKey, video: Video) =
    TvUseCases.isLastEpisode(repos.first, key, video.series, video.episode)

internal fun isLastReturningEpisode(repos: Pair<InfoRepo, CoordinatorRepo>, key: InfoKey, video: Video) =
    TvUseCases.isLastReturningEpisode(repos.first, key, video.series, video.episode)

internal fun isLastEpisodeInSeries(repos: Pair<InfoRepo, CoordinatorRepo>, key: InfoKey, video: Video): Boolean =
    isLastEpisodeInSeries(repos.first, key, video.series, video.episode)

internal fun registerLastEpisode(repo: CoordinatorRepo, videos: Pair<Video, Video>) {
    CoordinatorUseCases.getRepo(repo, Finished.name).register(videos.second)
    CoordinatorUseCases.getRepo(repo, Next.name).remove(videos.first)
}

internal fun registerLastReturningEpisode(repo: CoordinatorRepo, videos: Pair<Video, Video>) {
    CoordinatorUseCases.getRepo(repo, Paused.name).register(videos.second)
    CoordinatorUseCases.getRepo(repo, Next.name).remove(videos.first)
}

internal fun registerLastEpisodeInSeries(repo: CoordinatorRepo, videos: Pair<Video, Video>) {
    val newVideo = videos.first.copy(series = videos.first.series + 1, episode = 1)
    CoordinatorUseCases.getRepo(repo, Next.name).register(newVideo)
}

internal fun registerNextEpisode(repo: CoordinatorRepo, videos: Pair<Video, Video>) {
    val newVideo = videos.first.copy(episode = videos.first.episode + 1)
    CoordinatorUseCases.getRepo(repo, Next.name).register(newVideo)
}
